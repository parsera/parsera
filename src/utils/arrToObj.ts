const arrToObj = (arr: Array<any & { id: string }>) => {
  return arr.reduce((acc, value) => {
    return { ...acc, [value.id]: value }
  }, {})
}

export default arrToObj

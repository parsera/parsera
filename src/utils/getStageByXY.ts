import { type StageBlock } from '../classes/Block'

// Утилита используемая что-бы привязать блок к стейджу после
// того как мы перетащили блок и бросили над стеджом
const getStageByXY = (stages: StageBlock[], x: number, y: number): StageBlock | undefined => {
  return stages.find((stage) => {
    const { x: stageX, y: stageY, height, width } = stage
    if ((x > stageX && x < stageX + width) && (y > stageY && y < stageY + height)) return true
    return false
  })
}

export default getStageByXY

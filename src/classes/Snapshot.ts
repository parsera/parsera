import { type StageBlock } from './Block'
import type Block from './Block'
import Editor from './Editor'
import Input from './Input'
import type Link from './Link'
import Output from './Output'

export interface State {
  blocks: Block[]
  stages: StageBlock[]
  links: Link[]
}

class Snapshot {
  state: State
  constructor (blocks: Block[], stages: StageBlock[], links: Link[]) {
    this.state = {
      blocks: structuredClone(blocks),
      stages: structuredClone(stages),
      links: structuredClone(links)
    }
  }

  getState (): State {
    return this.state
  }
}

export default Snapshot

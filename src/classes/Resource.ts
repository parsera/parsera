interface Resource {
  id: string
  hasResource: string
  type: string
  value: string
  name: string
  createdBy: string
}

export default Resource

import { v4 } from 'uuid'
import End from './End'

class Link {
  id: string
  inputId: string
  outputId: string
  ends: [End, End] = [new End(0, 0), new End(0, 0)]
  constructor (point1: End, point2: End, inputId: string, output: string) {
    this.id = v4()
    this.ends = [point1, point2]
    this.inputId = inputId
    this.outputId = output
  }
}

export default Link

interface IElement {
  type: string
  x: number
  y: number
}

class EditorElement implements IElement {
  type: string
  x: number
  y: number

  constructor (x: number, y: number, type: string) {
    this.x = x
    this.y = y
    this.type = type
  }
}

export default EditorElement

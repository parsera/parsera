import { v4 } from 'uuid'
import { BlockTypes } from '../constants'
import Point from '../classes/Point'
import type End from './End'

export type BlockId = string

interface Command {
  resourceType: string
  userParams: Record<string, string>
  code: string
}

interface IBlock {
  id: BlockId
  x: number
  y: number
  scale: number
  width: number
  height: number
  text: string
  type: string
  inputXY: Point
  outputXY: Point
  inputEnds: End[]
  outputEnds: End[]
  inputs: string[]
  outputs: string[]
  command: Command
}

class Block implements IBlock {
  id: BlockId
  x: number
  y: number
  scale: number
  width: number
  height: number
  text: string
  type: string
  inputXY: Point
  outputXY: Point
  inputEnds: End[]
  outputEnds: End[]
  inputs: string[]
  outputs: string[]
  command: Command
  blockType: string = 'block'
  start: boolean = false
  end: boolean = false
  links: string[] = []

  constructor (type: string, x: number = 250, y: number = 100, width: number = 200, height: number = 50) {
    this.id = v4()
    this.type = type
    this.x = x
    this.y = y
    this.scale = 0.75
    this.width = width * this.scale
    this.height = height * this.scale
    this.text = ''
    this.inputEnds = []
    this.outputEnds = []
    this.inputXY = new Point(this.x + this.width / 2 - 10, this.y - 10)
    this.outputXY = new Point(this.x + this.width / 2 - 10, this.y + this.height - 10)
    this.inputs = []
    this.outputs = []
    this.command = {
      resourceType: '',
      userParams: {},
      code: ''
    }
  }
}

class InitBrowserBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.InitBrowser, ...params)
    this.command.resourceType = 'browser'
    this.command.code = '(function () { return async function initBrowser (resouces, userParams) {' +
                                'const browser = await puppeteer.launch({ headless: true, defaultViewport: null });' +
                                'return browser;' +
                            '} })();'
  }
}

class OpenPageBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.OpenPage, ...params)

    this.command.resourceType = 'page'
    this.command.userParams = {
      url: 'https://romanpushtuk.github.io/cv-page/'
    }
    this.command.code = '(function () { return async function openPage (resouces, userParams) {' +
                                'const { browser } = resouces;' +
                                'const { url } =  userParams;' +
                                'const page = await browser.newPage();' +
                                "await page.goto(url, { timeout: 5000, waitUntil: ['networkidle0'] });" +
                                'return page;' +
                            '} })();'
  }
}

class TakeElementBySelectorBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.TakeElementBySelector, ...params)
    this.command.resourceType = 'element'
    this.command.userParams = {
      selector: '.header-info_header-name'
    }
    this.command.code = '(function () { return async function getElement (resouces, userParams) {' +
                                'const { page } = resouces;' +
                                'const { selector } =  userParams;' +
                                'const element = await page.$(selector);' +
                                'return element;' +
                            '} })();'
  }
}

class ExtractInnerTextBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.ExtractInnerText, ...params)
    this.command.resourceType = 'text1'
    this.command.code = '(function () { return async function extractText (resouces, userParams) {' +
                                'const { element } = resouces;' +
                                "const text = await (await element.getProperty('textContent')).jsonValue();" +
                                'return text;' +
                            '} })();'
  }
}

class BucketBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.MergeResults, ...params)
    this.command.resourceType = 'bucket'
    this.command.code = '(function () { return async function bucket (resouces, userParams) {' +
                                'return resouces;' +
                            '} })();'
  }
}

class ClosePageBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.ClosePage, ...params)
    this.command.code = '(function () { return async function closePage (resouces, userParams) {' +
                                'const { page } = resouces;' +
                                'await page.close();' +
                                'return { markResourceAsInactive: page };' +
                            '} })();'
  }
}

class CloseBrowserBlock extends Block {
  constructor (...params: any[]) {
    super(BlockTypes.CloseBrowser, ...params)
    this.command.code = '(function () { return async function closeBrowser (resouces, userParams) {' +
                                'const { browser } = resouces;' +
                                'await browser.close();' +
                                'return { markResourceAsInactive: browser };' +
                            '} })();'
  }
}

class StageBlock extends Block {
  blocks: BlockId[] = []
  blockType = 'stage'
  start: boolean = false
  end: boolean = false

  constructor (...params: any[]) {
    super(BlockTypes.Stage, ...params)
    this.x = 235
    this.y = 70
    this.height = 80
    this.width = 450

    this.inputXY = new Point(this.x + this.width - 10, this.y - 10)
    this.outputXY = new Point(this.x + this.width - 10, this.y + this.height - 10)
  }
}

export {
  InitBrowserBlock,
  OpenPageBlock,
  TakeElementBySelectorBlock,
  ExtractInnerTextBlock,
  BucketBlock,
  StageBlock,
  CloseBrowserBlock,
  ClosePageBlock
}
export default Block

import path from 'path'
import { JsonDB, Config } from 'node-json-db'
import type CreateUserDTO from '@/dto/CreateUserDTO'
import type ChangeUserDataDTO from '@/dto/ChangeUserDataDTO'

const dbPath = path.join(process.cwd(), 'parseraDatabase.json')

class Db {
  private static readonly db = new JsonDB(new Config(dbPath, true, true, '/'))

  static async getParser (id: string) {
    return await Db.db.getData(`/parsers/${id}`)
  }

  static async getParsers () {
    try {
      const map = Db.db.getData(`/parsers`)
      return Object.values(map)
    } catch (err) {
      console.log(err)
      return null
    }
  }

  static async deleteParser (id: string) {
    await Db.db.delete(`/parsers/${id}`)
  }

  static async getUser (data: string) {
    return await Db.db.getData(`/users/${data}`)
  }

  static async createUser (data: CreateUserDTO) {
    try {
      await Db.db.push(`/users/${data.login}`, data)
    } catch (err) {
      console.log(err)
      return null
    }
  }

  static async changeUserData(data:ChangeUserDataDTO){
    try {
        const updateData = await this.getUser(data.login)
        updateData.password=data.change
        await Db.db.delete(`users/${updateData.login}`)
        return await Db.db.push(`/users/${updateData.email}/`,updateData)
    } catch (err) {
        console.log(err)
        return null
    }
}
}

export default Db

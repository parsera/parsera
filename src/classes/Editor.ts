import { BlockTypes } from '../constants'
import { type BlockId } from './Block'
import type Block from './Block'
import {
  InitBrowserBlock,
  OpenPageBlock,
  TakeElementBySelectorBlock,
  ExtractInnerTextBlock,
  BucketBlock,
  StageBlock,
  CloseBrowserBlock,
  ClosePageBlock
} from './Block'
import EditorElement from './Element'
import type Command from './commands/Command'
import type Link from './Link'
import MoveBlockCommand from './commands/MoveBlockCommand'
import AddBlockBlock from './commands/AddBlockCommand'
import AddLinkCommand from './commands/AddLinkCommand'
import DeleteBlockCommand from './commands/DeleteBlockCommand'
import AddBlockToStageCommand from './commands/AddBlockToStageCommand'
import AddStageBlockCommand from './commands/AddStageBlockCommand'
import ChangeBlockDefaultParamsCommand from './commands/ChangeBlockDefaultParamsCommand'
import ChangeBlockUserParamsCommand from './commands/ChangeBlockUserParamsCommand'
import addBlock from './operations/addBlock'
import moveBlock from './operations/moveBlock'
import addLink from './operations/addLink'
import deleteBlock from './operations/deleteBlock'
import addStageBlock from './operations/addStageBlock'
import addBlockToStage from './operations/addBlockToStage'
import changeDefaultParams from './operations/changeDefaultParams'
import changeUserParams from './operations/changeUserParams'
import ExecuteBlockCommand from './commands/ExecuteBlockCommand'
import executeBlock from './operations/executeBlock'
// import ChangeCreatedResourcesCommand from "./commands/ChangeCreatedResourcesCommand"
// import ChangeResourcesFromPrevStageCommand from "./commands/ChangeResourcesFromPrevStageCommand"
// import changeCreatedResources from "./operations/changeCreatedResources"
// import changeResourcesFromPrevStage from "./operations/changeResourcesFromPrevStage"
import type Input from './Input'
import type Output from './Output'
import Snapshot from './Snapshot'
import EditorHistory from './EditorHistory'
import ChangeScaleCommand from './commands/ChangeScaleCommand'
import changeScale from './operations/changeScale'
import type EditorParams from '../types/EditorParams'

const typeBlockClasses = {
  [BlockTypes.InitBrowser]: InitBrowserBlock,
  [BlockTypes.OpenPage]: OpenPageBlock,
  [BlockTypes.TakeElementBySelector]: TakeElementBySelectorBlock,
  [BlockTypes.ExtractInnerText]: ExtractInnerTextBlock,
  [BlockTypes.MergeResults]: BucketBlock,
  [BlockTypes.Stage]: StageBlock,
  [BlockTypes.ClosePage]: ClosePageBlock,
  [BlockTypes.CloseBrowser]: CloseBrowserBlock
}

export type Subscriber = (data: {
  params: EditorParams
  blocks: Block[]
  links: Link[]
  stages: StageBlock[]
}) => void

class Editor {
  static params: EditorParams = {
    blockListMenuWidth: 200,
    resourcesMenuWidth: 200,
    blockParamsMenuWidth: 200
  }

  static blocks: Block[] = []
  static stages: StageBlock[] = []
  static links: Link[] = []
  static inputs: Input[] = []
  static outputs: Output[] = []
  static subscribers: Subscriber[] = []

  static get blocksAndStages () {
    return [...Editor.blocks, ...Editor.stages]
  }

  static getBlock (id: string) {
    return Editor.blocks.find((block) => block.id == id)
  }

  static getStage (id: string) {
    return Editor.stages.find((stage) => stage.id == id)
  }

  static getBlockOrStage (id: string) {
    console.log(Editor.getBlock(id), Editor.getStage(id))
    return Editor.getBlock(id) ?? Editor.getStage(id)
  }

  static addBlock (block: Block) {
    Editor.blocks.push(block)
  }

  static getBlockInputs (blockId: BlockId) {
    return Editor.inputs.filter((input) => {
      return input.ownBlockId === blockId
    })
  }

  static getBlockOutputs (blockId: BlockId) {
    return Editor.outputs.filter((output) => {
      return output.ownBlockId === blockId
    })
  }

  static addInputs (inputs: Input[]) {
    Editor.inputs = [...Editor.inputs, ...inputs]
  }

  static getInput (id: string) {
    return Editor.inputs.find((input) => input.id == id)
  }

  static addOutputs (outputs: Output[]) {
    Editor.outputs = [...Editor.outputs, ...outputs]
  }

  static getOutput (id: string) {
    return Editor.outputs.find((outputs) => outputs.id == id)
  }

  static addStage (stage: StageBlock) {
    Editor.stages.push(stage)
  }

  static addLink (link: Link) {
    Editor.links.push(link)
  }

  static subscribe (subscriber: Subscriber) {
    Editor.subscribers.push(subscriber)
  }

  static unsubscribe (subscriber: Subscriber) {
    Editor.subscribers = Editor.subscribers.filter((sub) => subscriber !== sub)
  }

  static emit () {
    Editor.subscribers.forEach((subscriber) => {
      subscriber({
        blockListMenuWidth: Editor.blockListMenuWidth,
        resourcesMenuWidth: Editor.blockListMenuWidth,
        blockParamsMenuWidth: Editor.blockParamsMenuWidth,

        blocks: Editor.blocks,
        links: Editor.links,
        stages: Editor.stages
      })
    })
  }

  static createSnapshot (): Snapshot {
    return new Snapshot(Editor.blocks, Editor.stages, Editor.links, Editor.inputs, Editor.outputs)
  }

  static restore (backup: Snapshot) {
    console.log('Восстановили состояние')
    const { blocks, stages, links, inputs, outputs } = backup.getState()
    Editor.blocks = blocks
    Editor.stages = stages
    Editor.links = links
    Editor.inputs = inputs
    Editor.outputs = outputs
    Editor.emit()
  }

  static executeCommand (command: Command) {
    if (!(command instanceof MoveBlockCommand)) {
      command.makeBackup()
      EditorHistory.push(command)
      console.log('Сделали бэкап команды, и отправили в историю')
    }

    if (command instanceof AddBlockBlock) {
      addBlock(command)
    }
    if (command instanceof MoveBlockCommand) {
      moveBlock(command)
    }
    if (command instanceof AddLinkCommand) {
      addLink(command)
    }
    if (command instanceof DeleteBlockCommand) {
      deleteBlock(command)
    }
    if (command instanceof AddStageBlockCommand) {
      addStageBlock(command)
    }
    if (command instanceof AddBlockToStageCommand) {
      addBlockToStage(command)
    }
    if (command instanceof ChangeBlockDefaultParamsCommand) {
      changeDefaultParams(command)
    }
    if (command instanceof ChangeBlockUserParamsCommand) {
      changeUserParams(command)
    }
    if (command instanceof ExecuteBlockCommand) {
      executeBlock(command)
    }
    if (command instanceof ChangeScaleCommand) {
      changeScale(command)
    }
    // if(command instanceof ChangeCreatedResourcesCommand) {
    //     changeCreatedResources(command)
    // }
    // if(command instanceof ChangeResourcesFromPrevStageCommand) {
    //     changeResourcesFromPrevStage(command)
    // }
  }
}

export { typeBlockClasses }
export default Editor

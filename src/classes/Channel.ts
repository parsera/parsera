import executeBlockAction from './actions/executeBlockAction'
import executeStageAction from './actions/executeStageAction'

type Subscriber = (data: any) => void

class Channel {
  static ws: WebSocket
  static subscribers: Subscriber[] = []

  static init () {
    Channel.ws = new WebSocket('ws://localhost:8880')
    Channel.ws.addEventListener('error', Channel.onError)
    Channel.ws.addEventListener('open', Channel.onOpen)
    Channel.ws.addEventListener('message', Channel.onMessage)
    Channel.ws.addEventListener('close', Channel.onClose)
  }

  static subscribe (subscriber: Subscriber) {
    Channel.subscribers.push(subscriber)
  }

  static send (data: any) {
    if (Channel.ws.readyState != 3) {
      Channel.ws.send(JSON.stringify(data))
    }
  }

  static emit (data: any) {
    Channel.subscribers.forEach((subscriber) => {
      subscriber(data)
    })
  }

  static executeBlock (data: any) {
    if (Channel.ws.readyState != 3) {
      const action = executeBlockAction(data)
      Channel.ws.send(JSON.stringify(action))
    }
  }

  static executeStage (blockId: string) {
    if (Channel.ws.readyState != 3) {
      const action = executeStageAction(blockId)
      Channel.ws.send(JSON.stringify(action))
    }
  }

  static onMessage (event: MessageEvent) {
    console.log(`[message] Данные получены с сервера: ${event.data}`)

    const action = JSON.parse(event.data)

    const { type, data } = action as { type: string, data: any }

    if (type == 'BLOCK_EXECUTION_RESULT') {
      Channel.emit(data)
    }
  }

  static onError (event: Event) {
    console.error(event)
  }

  static onOpen (event: Event) {
    console.log('Открыли конекшн')
  }

  static onClose (event: CloseEvent) {
    console.log('закрыли')
  }
}

export default Channel

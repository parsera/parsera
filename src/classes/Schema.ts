import { type StageBlock } from './Block'
import type Block from './Block'
import ResourceManager from './ResourceManager'

class Schema {
  blocksMap: Map<any, any>
  stagesMap: Map<any, any>
  startStage: StageBlock | null
  endStage: StageBlock | null

  constructor (blocks: Array<Block | StageBlock>) {
    this.blocksMap = new Map()
    this.stagesMap = new Map()

    this.startStage = null
    this.endStage = null

    blocks.forEach((block) => {
      if (block.start) this.startStage = block as StageBlock
      if (block.end) this.endStage = block as StageBlock

      if (block.blockType == 'block') {
        this.blocksMap.set(block.id, block)
      }

      if (block.blockType == 'stage') {
        this.stagesMap.set(block.id, block)
      }
    })
  }

  getStage (id: string) {
    return this.stagesMap.get(id)
  }

  getBlock (id: string) {
    return this.blocksMap.get(id)
  }

  async iteratingInWidth (cb: (stage: StageBlock) => void) {
    const queue = []
    if (!this.startStage) return
    queue.push(this.startStage)
    await cb(this.startStage)
    while (queue.length > 0) {
      const stage = queue.shift()
      for (const stageId of stage.outputs) {
        const nextStage = this.getStage(stageId)
        queue.push(nextStage)
        await cb(nextStage)
      }
    }

    return false
  }

  async execute () {
    await this.iteratingInWidth(async (stage) => {
      if (this.startStage && stage.id === this.startStage.id) {
        console.log('Начали исполнение')
      }

      if (this.endStage && stage.id === this.endStage.id) {
        console.log('Завершили выполнение')
      }

      console.log('Исполняем stage -', stage.id)

      if (stage.blocks) {
        await this.executeStage(stage.id)
      } else {
        // Если стейдж не содержит блоков, то будем считать
        // что это тоже блок
        await this.executeBlock(stage.id)
      }
    })

    return ResourceManager.view()
  }

  async executeStage (stageId: string) {
    const stage = this.getStage(stageId)

    for (const id of stage.blocks) {
      await this.executeBlock(id)
    }

    return ResourceManager.view()
  }

  async executeBlock (blockId: string) {
    console.log('Исполняем блок -', blockId)
    const block = this.getBlock(blockId)
    await ResourceManager.exec(block)
    return ResourceManager.view()
  }
}

export default Schema

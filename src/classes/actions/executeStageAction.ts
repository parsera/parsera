const executeStageAction = (blockId: string) => {
  const action = {
    type: 'EXECUTE_STAGE',
    data: {
      blockId
    }
  }
  return action
}

export default executeStageAction

const executeBlockAction = (data: any): any => {
  const action = {
    type: 'EXECUTE_BLOCK',
    data
  }
  return action
}

export default executeBlockAction

import puppeteer from 'puppeteer'
import type Block from './Block'

class ResourceManager {
  static resourceMap = new Map()

  static async exec (block: Block) {
    const { id, command, inputs } = block
    const { code, resourceType, userParams } = command

    const resources = ResourceManager.findResourses(inputs)
    const func = eval(code)
    const newResource = await func(resources, userParams)

    if (newResource.markResourceAsInactive) {
      ResourceManager.markResourceAsInactive(newResource.markResourceAsInactive)
    } else {
      ResourceManager.set(id, { value: newResource, type: resourceType })
    }
  }

  static markResourceAsInactive (resource: any) {
    for (const resourceMapResource of ResourceManager.resourceMap.values()) {
      if (resourceMapResource.value === resource) {
        resourceMapResource.inactive = true
      } else {
        continue
      }
    }
  }

  static findResourses (inputs: string[]) {
    return inputs.reduce((acc, blockId) => {
      const resource = ResourceManager.resourceMap.get(blockId)
      if (!resource) {
        throw new Error(`Не найден ресурс - ${blockId}. Прежде чем исполнить блок нужно убедится что созданы все необходимые ресурсы.`)
      }
      const { type, value } = resource
      return { ...acc, [type]: value }
    }, {})
  }

  static view () {
    return Object.fromEntries(ResourceManager.resourceMap)
  }

  static get (blockId: string) {
    return ResourceManager.resourceMap.get(blockId)
  }

  static set (blockId: string, resource: any) {
    ResourceManager.resourceMap.set(blockId, resource)
  }

  static delete (blockId: string) {
    ResourceManager.resourceMap.delete(blockId)
  }
}

export default ResourceManager

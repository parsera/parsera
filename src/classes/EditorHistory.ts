import Snapshot from './Snapshot'
import type Command from './commands/Command'

class EditorHistory {
  static history: Command[] = []

  static push (command: Command) {
    EditorHistory.history.push(command)
  }

  static getLastCommand (): Command | undefined {
    return EditorHistory.history.pop()
  }
}

export default EditorHistory

import Editor from '../Editor'
import type ChangeCreatedResourcesCommand from '../commands/ChangeCreatedResourcesCommand'

const changeCreatedResources = (command: ChangeCreatedResourcesCommand) => {
  const { blockId, data } = command
  const block = Editor.getBlockOrStage(blockId)
  block.createdResources = { ...data }
  Editor.emit()
}

export default changeCreatedResources

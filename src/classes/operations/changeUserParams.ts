import Editor from '../Editor'
import type ChangeBlockUserParamsCommand from '../commands/ChangeBlockUserParamsCommand'

const changeUserParams = (command: ChangeBlockUserParamsCommand) => {
  const { blockId, keyName, value } = command
  const block = Editor.getBlockOrStage(blockId)
  if (!block) return
  block.command.userParams[keyName] = value
  Editor.emit()
}

export default changeUserParams

import Editor from '../Editor'
import type AddBlockToStageCommand from '../commands/AddBlockToStageCommand'

const addBlockToStage = (command: AddBlockToStageCommand) => {
  const { stageId, blockId } = command
  const stage = Editor.getStage(stageId)
  // const block = Editor.getBlock(blockId)
  stage.blocks.push(blockId)
  Editor.emit()
}

export default addBlockToStage

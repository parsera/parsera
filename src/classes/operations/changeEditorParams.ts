import Editor from '../Editor'
import type ChangeEditorParamsCommand from '../commands/ChangeEditorParamsCommand'
import ChangeResourcesFromPrevStageCommand from '../commands/ChangeResourcesFromPrevStageCommand'

const changeEditorParams = (command: ChangeEditorParamsCommand) => {
  const { keyName, value } = command
  Editor[keyName] = value
  Editor.emit()
}

export default changeEditorParams

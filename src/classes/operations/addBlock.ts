import { BlockTypes } from '../../constants'
import Editor, { typeBlockClasses } from '../Editor'
import type AddBlockCommand from '../commands/AddBlockCommand'
import { type StageBlock } from '../Block'
import EditorHistory from '../EditorHistory'

const addBlock = (command: AddBlockCommand) => {
  const { blockType } = command
  const BlockClass = typeBlockClasses[blockType]
  const block = new BlockClass()

  if (blockType === BlockTypes.Stage) {
    Editor.addStage(block as StageBlock)
  } else {
    Editor.addBlock(block)
  }
  Editor.emit()
}

export default addBlock

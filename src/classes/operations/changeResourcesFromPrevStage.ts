import Editor from '../Editor'
import type ChangeResourcesFromPrevStageCommand from '../commands/ChangeResourcesFromPrevStageCommand'

const changeResourcesFromPrevStage = (command: ChangeResourcesFromPrevStageCommand) => {
  const { blockId, data } = command
  const block = Editor.getBlockOrStage(blockId)
  block.resourcesFromPrevStage = { ...data }
  Editor.emit()
}

export default changeResourcesFromPrevStage

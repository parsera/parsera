import type Block from '../Block'
import { StageBlock } from '../Block'
import Editor from '../Editor'
import DeleteBlockCommand from '../commands/DeleteBlockCommand'

const deleteBlock = (command: DeleteBlockCommand) => {
  const { blockId } = command
  console.log(blockId)
  const block = Editor.getBlockOrStage(blockId)

  if (!block) return

  block.inputs.forEach((inputBlockId) => {
    const inputBlock = Editor.getBlockOrStage(inputBlockId) as Block
    inputBlock.outputs = inputBlock.outputs.filter((id) => id !== blockId)
  })

  block.outputs.forEach((outputBlockId) => {
    const outputBlock = Editor.getBlockOrStage(outputBlockId) as Block
    outputBlock.inputs = outputBlock.inputs.filter((id) => id !== blockId)
  })

  // Удаляем все линки которые связаны с удаляемым блоком
  Editor.links = Editor.links.filter((link) => {
    return !block.links.includes(link.id)
  })

  // Если это стейдж, то удаляем также блоки внутри его
  if (block instanceof StageBlock) {
    block.blocks.forEach((id) => {
      const command = new DeleteBlockCommand(id)
      deleteBlock(command)
    })

    Editor.stages = Editor.stages.filter((filterStage) => filterStage.id !== blockId)
  }

  Editor.blocks = Editor.blocks.filter((filterBlock) => filterBlock.id !== blockId)

  Editor.emit()
}

export default deleteBlock

import type Block from '../Block'
import { StageBlock } from '../Block'
import Editor from '../Editor'
import MoveBlockCommand from '../commands/MoveBlockCommand'

const moveBlock = (command: MoveBlockCommand) => {
  const { id, newX, newY } = command
  const block = Editor.getBlockOrStage(id) as Block
  if (!block) return
  const oldX = block.x
  const oldY = block.y

  const offsetX = newX - oldX
  const offsetY = newY - oldY

  block.x += offsetX
  block.y += offsetY

  block.inputXY.x += offsetX
  block.inputXY.y += offsetY

  block.outputXY.x += offsetX
  block.outputXY.y += offsetY

  const blockEnds = [...block.inputEnds, ...block.outputEnds]

  blockEnds.forEach((end) => {
    end.x += offsetX
    end.y += offsetY
  })

  // const elements = [...block.inputs, ...block.outputs]

  // elements.forEach((element: any) => {
  //     element.x+=offsetX
  //     element.y+=offsetY

  //     element.end.x+=offsetX
  //     element.end.y+=offsetY
  // })

  // check if it's StageBlock
  if (block instanceof StageBlock) {
    block.blocks.forEach((blockId) => {
      const block = Editor.getBlock(blockId)
      if (block) {
        const command = new MoveBlockCommand(block.id, block.x + offsetX, block.y + offsetY)
        moveBlock(command)
      }
    })
  }

  Editor.emit()
}

export default moveBlock

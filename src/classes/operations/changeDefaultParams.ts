import Editor from '../Editor'
import type ChangeBlockDefaultParamsCommand from '../commands/ChangeBlockDefaultParamsCommand'

const changeDefaultParams = (command: ChangeBlockDefaultParamsCommand) => {
  const { blockId, keyName, value } = command
  const block = Editor.getBlockOrStage(blockId)
  block[keyName] = value
  Editor.emit()
}

export default changeDefaultParams

import { BlockTypes } from '../../constants'
import Editor, { typeBlockClasses } from '../Editor'
import type AddStageBlockCommand from '../commands/AddStageBlockCommand'

const addStageBlock = (command: AddStageBlockCommand) => {
  const StageBlockClass = typeBlockClasses[BlockTypes.Stage]
  const stage = new StageBlockClass()
  Editor.addStage(stage)
  Editor.emit()
}

export default addStageBlock

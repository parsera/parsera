import Link from '../Link'
import Editor from '../Editor'
import End from '../End'
import type AddLinkCommand from '../commands/AddLinkCommand'
import { BlockTypes } from '../../constants'

const addLink = (command: AddLinkCommand) => {
  const { inputId, x1, y1, outputId, x2, y2 } = command

  // const input = Editor.getInput(inputId) as Input
  const inputBlock = Editor.getBlockOrStage(inputId)

  // const output = Editor.getOutput(outputId) as Output
  const outputBlock = Editor.getBlockOrStage(outputId)

  if (!outputBlock || !inputBlock) return

  const areInputAndOutputStages = inputBlock?.type === BlockTypes.Stage && outputBlock?.type === BlockTypes.Stage
  const areInputAndOutputNotStages = inputBlock?.type !== BlockTypes.Stage && outputBlock?.type !== BlockTypes.Stage

  if (areInputAndOutputStages || areInputAndOutputNotStages) {
    const end1 = new End(x1, y1, 'input')
    const end2 = new End(x2, y2, 'usual')

    const link = new Link(end1, end2, inputId, outputId)

    outputBlock?.outputEnds.push(end2)
    outputBlock?.outputs.push(inputBlock.id)

    inputBlock?.inputEnds.push(end1)
    inputBlock?.inputs.push(outputBlock.id)

    // input.end = end1
    // output.end = end2

    // output.next = [...output.next, input.id]
    // input.prev = [...input.prev, output.id]

    inputBlock?.links.push(link.id)
    outputBlock?.links.push(link.id)

    Editor.addLink(link)
    Editor.emit()
  }
}

export default addLink

import type ExecuteBlockCommand from '../commands/ExecuteBlockCommand'
import Channel from '../Channel'
import Editor from '../Editor'
import executeBlockAction from '../actions/executeBlockAction'
import arrToObj from '../../utils/arrToObj'

const executeBlock = (command: ExecuteBlockCommand) => {
  const { blockId } = command

  const data = {
    blockId,
    blocks: arrToObj(Editor.blocks),
    stages: arrToObj(Editor.stages),
    inputs: arrToObj(Editor.inputs),
    outputs: arrToObj(Editor.outputs)
  }

  const action = executeBlockAction(data)
  console.log('отправили экшн', action)
  Channel.send(action)
}

export default executeBlock

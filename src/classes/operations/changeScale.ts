import Block, { StageBlock } from '../Block'
import Editor, { typeBlockClasses } from '../Editor'
import type ChangeScaleCommand from '../commands/ChangeScaleCommand'

const changeScale = (command: ChangeScaleCommand) => {
  const { scale } = command

  Editor.blocks = Editor.blocks.map((block) => {
    const newWidth = block.width * scale
    const newHeight = block.height * scale
    const BlockClass = typeBlockClasses[block.type]
    const newBlock = new BlockClass(block.x, block.y, newWidth, newHeight)
    return newBlock
  })

  Editor.stages = Editor.stages.map((block) => {
    const newWidth = block.width * scale
    const newHeight = block.height * scale
    const newBlock = new StageBlock(block.x, block.y, newWidth, newHeight)
    return newBlock
  })
  Editor.emit()
}

export default changeScale

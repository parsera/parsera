import type Snapshot from '../Snapshot'
import Editor from '../Editor'

interface ICommand {

}

class Command implements ICommand {
  private backup: Snapshot | null = null

  makeBackup () {
    this.backup = Editor.createSnapshot()
  }

  undo () {
    if (this.backup != null) {
      Editor.restore(this.backup)
    }
  }
}

export default Command

import type Editor from '../Editor'
import Command from './Command'

class ChangeEditorParamsCommand extends Command {
  keyName: keyof typeof Editor
  value: string
  constructor (keyName: keyof typeof Editor, value: string) {
    super()
    this.keyName = keyName
    this.value = value
  }
}

export default ChangeEditorParamsCommand

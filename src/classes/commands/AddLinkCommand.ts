import Command from './Command'

class AddLinkCommand extends Command {
  inputId: string
  x1: number
  y1: number

  outputId: string
  x2: number
  y2: number

  constructor (inputId: string, x1: number, y1: number, outputId: string, x2: number, y2: number) {
    super()
    this.inputId = inputId
    this.x1 = x1
    this.y1 = y1
    this.outputId = outputId
    this.x2 = x2
    this.y2 = y2
  }
}

export default AddLinkCommand

import { BlockTypes } from '../../constants'
import Command from './Command'

class ExecuteBlockCommand extends Command {
  blockId: string
  constructor (blockId: string) {
    super()
    this.blockId = blockId
  }
}

export default ExecuteBlockCommand

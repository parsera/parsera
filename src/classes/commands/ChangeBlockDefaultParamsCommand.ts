import Command from './Command'

class ChangeBlockDefaultParamsCommand extends Command {
  blockId: string
  keyName: string
  value: number | string | boolean
  constructor (blockId: string, keyName: string, value: number | string | boolean) {
    super()
    this.blockId = blockId
    this.keyName = keyName
    this.value = value
  }
}

export default ChangeBlockDefaultParamsCommand

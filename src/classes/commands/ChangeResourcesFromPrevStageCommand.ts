import Command from './Command'

class ChangeResourcesFromPrevStageCommand extends Command {
  blockId: string
  data: any
  constructor (blockId: string, data: any) {
    super()
    this.blockId = blockId
    this.data = data
  }
}

export default ChangeResourcesFromPrevStageCommand

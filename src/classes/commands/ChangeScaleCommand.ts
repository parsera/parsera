import Command from './Command'

class ChangeScaleCommand extends Command {
  scale: number
  constructor (scale: number) {
    super()
    this.scale = scale
  }
}

export default ChangeScaleCommand

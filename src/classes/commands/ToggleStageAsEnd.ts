import Command from './Command'

class ToggleStageAsEnd extends Command {
  blockId: string
  constructor (blockId: string) {
    super()
    this.blockId = blockId
  }
}

export default ToggleStageAsEnd

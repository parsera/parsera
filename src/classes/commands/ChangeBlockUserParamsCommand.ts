import { BlockTypes } from '../../constants'
import Command from './Command'

class ChangeBlockUserParamsCommand extends Command {
  blockId: string
  keyName: string
  value: string
  constructor (blockId: string, keyName: string, value: string) {
    super()
    this.blockId = blockId
    this.keyName = keyName
    this.value = value
  }
}

export default ChangeBlockUserParamsCommand

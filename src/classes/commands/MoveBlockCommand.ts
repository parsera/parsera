import Command from './Command'

class MoveBlockCommand extends Command {
  id: string
  newX: number
  newY: number
  constructor (id: string, newX: number, newY: number) {
    super()
    this.id = id
    this.newX = newX
    this.newY = newY
  }
}

export default MoveBlockCommand

import { type BlockTypes } from '../../constants'
import Command from './Command'

class AddBlockCommand extends Command {
  blockType: BlockTypes
  constructor (blockType: BlockTypes) {
    super()
    this.blockType = blockType
  }
}

export default AddBlockCommand

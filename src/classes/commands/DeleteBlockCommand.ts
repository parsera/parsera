import Command from './Command'

class DeleteBlockCommand extends Command {
  blockId: string
  constructor (blockId: string) {
    super()
    this.blockId = blockId
  }
}

export default DeleteBlockCommand

import Command from './Command'

class AddBlockToStage extends Command {
  stageId: string
  blockId: string
  constructor (stageId: string, blockId: string) {
    super()
    this.stageId = stageId
    this.blockId = blockId
  }
}

export default AddBlockToStage

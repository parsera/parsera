import Command from './Command'

class ToggleStageAsStart extends Command {
  blockId: string
  constructor (blockId: string) {
    super()
    this.blockId = blockId
  }
}

export default ToggleStageAsStart

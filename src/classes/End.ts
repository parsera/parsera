import Point from './Point'

class End extends Point {
  type: string
  // x: number
  // y: number
  constructor (x: number, y: number, type: string = 'usual') {
    super(x, y)
    // this.x = x
    // this.y = y
    this.type = type
  }
}

export default End

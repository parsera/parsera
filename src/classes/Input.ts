import { v4 } from 'uuid'
import End from './End'

class Input {
  id: string
  x: number
  y: number
  output: string | string[]
  ownBlockId: string
  isMultiple: boolean
  end: End
  prev: string[]

  constructor (x: number, y: number, isMultiple: boolean, ownBlockId: string) {
    this.id = v4()
    this.x = x
    this.y = y
    this.output = ''
    this.ownBlockId = ''
    this.isMultiple = isMultiple
    this.ownBlockId = ownBlockId
    this.end = new End(0, 0)
    this.prev = []
  }
}

export default Input

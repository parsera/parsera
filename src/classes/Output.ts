import { v4 } from 'uuid'
import End from './End'

class Output {
  id: string
  x: number
  y: number
  output: string | string[]
  ownBlockId: string
  end: End
  next: string[]

  constructor (x: number, y: number, ownBlockId: string) {
    this.id = v4()
    this.x = x
    this.y = y
    this.output = ''
    this.ownBlockId = ''
    this.ownBlockId = ownBlockId
    this.end = new End(0, 0)
    this.next = []
  }
}

export default Output

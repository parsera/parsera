import MainSection from '../components/MainSection'
import FeaturesSection from '../components/FeaturesSection'

export default function HomePage () {
  return (
    <main>
      <MainSection />
      <FeaturesSection />
    </main>
  )
}

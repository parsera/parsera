'use client'
import { useState, useEffect } from 'react'
import _ from 'lodash'
import styles from './styles.module.scss'
import Editor from '../../../components/Editor'
import ControlsMenu from '../../../components/Controls'
import EditorFooter from '../../../components/EditorHeader'
import BlockMenu from '../../../components/Menu/Menu'
import Popup from '../../../components/Popup'
import ResourcesMenu from '../../../components/Resources'
import DragSplit from '../../../components/DragSplit'
import { type StageBlock } from '../../../classes/Block'
import type Block from '../../../classes/Block'
import getStageByXY from '../../../utils/getStageByXY'
import useEditor from '../../../hooks/useEditor'

const EditorPage = () => {
  const {
    err,
    resources,
    params,
    blocks,
    stages,
    links,
    back,
    execute,
    executeBlock,
    addBlock,
    deleteBlock,
    markAsStart,
    markAsEnd,
    addLink,
    moveBlock,
    addBlockToStage,
    save
  } = useEditor()

  const [menuBlock, setMenuBlock] = useState<Block | StageBlock | null>(null)
  const [dragBlock, setDragBlock] = useState<{ id: string, deltaX: number, deltaY: number } | null>(null)
  const [blockPopupMenu, setBlockPopupMenu] = useState<{ id: string, x: number, y: number } | null>(null)
  const [input, setInput] = useState<{ id: string, x: number, y: number } | null>(null)
  const [output, setOutput] = useState<{ id: string, x: number, y: number } | null>(null)

  useEffect(() => {
    const onKeyDown = (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === 'z') {
        back()
      }
    }

    document.addEventListener('keydown', onKeyDown)

    return () => {
      document.removeEventListener('keydown', onKeyDown)
    }
  }, [])

  const handleInputClick = (event: React.MouseEvent) => {
    event.stopPropagation()
    const id = (event.target as HTMLDivElement).getAttribute('data-block-id') as string
    const { x, y, width } = (event.target as HTMLDivElement).getBoundingClientRect()
    const inputX = x + (width / 2)
    const inputY = y

    if (output) {
      addLink(id, inputX, inputY, output.id, output.x, output.y)
      setInput(null)
    } else {
      setInput({ id, x: inputX, y: inputY })
    }
  }

  const handleOutputClick = (event: React.MouseEvent) => {
    event.stopPropagation()
    const id = (event.target as HTMLDivElement).getAttribute('data-block-id') as string
    const { x, y, width, height } = (event.target as HTMLDivElement).getBoundingClientRect()

    const outputX = x + (width / 2)
    const outputY = y + height

    if (input) {
      addLink(input.id, input.x, input.y, id, outputX, outputY)
      setOutput(null)
    } else {
      setOutput({ id, x: outputX, y: outputY })
    }
  }

  const handleDragStart = (event: React.MouseEvent) => {
    const id = (event.target as HTMLDivElement).getAttribute('data-block-id') as string
    const { x, y } = (event.target as HTMLDivElement).getBoundingClientRect()
    const { pageX, pageY } = event
    const deltaX = (pageX - x)
    const deltaY = (pageY - y)
    setDragBlock({ id, deltaX, deltaY })
  }

  const handleMouseMove = (event: React.MouseEvent) => {
    if (dragBlock) {
      const { pageX, pageY } = event
      const { id, deltaX, deltaY } = dragBlock
      const x = (pageX - deltaX)
      const y = (pageY - deltaY)
      moveBlock(id, x, y)
    }
  }

  const handleDragEnd = (event: React.MouseEvent) => {
    const { id } = dragBlock
    const { pageX, pageY } = event
    const droppableStage = getStageByXY(stages, pageX, pageY)
    if (
      id &&
            droppableStage &&
            droppableStage.id !== id &&
            !stages.find(({ id: stageId }) => stageId === id)
    ) {
      addBlockToStage(droppableStage.id, id)
    }
    setDragBlock(null)
  }

  const handleBlockClick = (event: React.MouseEvent) => {
    const id = (event.target as HTMLDivElement).getAttribute('data-block-id') as string
    const block = [...blocks, ...stages].find((block) => block.id == id)
    if (block) {
      setMenuBlock(block)
    }
  }

  const handleRightClick = (event: React.MouseEvent) => {
    event.preventDefault()
    const id = (event.target as HTMLDivElement).getAttribute('data-block-id') as string
    const { pageX, pageY } = event
    setBlockPopupMenu({ id, x: pageX, y: pageY })
  }

  const handleClickOnEmpty = (event: React.MouseEvent) => {
    setBlockPopupMenu(null)
  }

  const handleExecucuteBlock = (id: string) => {
    executeBlock(id)
    setBlockPopupMenu(null)
  }

  const handleDeleteBlock = (id: string) => {
    deleteBlock(id)
    setBlockPopupMenu(null)
  }

  const handleSave = () => {
    save()
  }

  const handleMarkAsStart = (id: string) => {
    markAsStart(id)
    setBlockPopupMenu(null)
  }

  const handleMarkAsEnd = (id: string) => {
    markAsEnd(id)
    setBlockPopupMenu(null)
  }

  const handleChangeControlsMenuWidth = (width: number) => {
    // setBlockListMenuWidth(width)
  }

  const handleSplitChangeResourcesMenuWidth = (width: number) => {
    // setResourcesMenuWidth(width)
  }

  const handleChangeBlockMenuWidth = (width: number) => {
    // setBlockParamsMenuWidth(width)
  }

  return (
        <div className={styles.container} >
            <ControlsMenu
                width={params.blockListMenuWidth}
                onAddBlock={addBlock}
            />
            <DragSplit left={true} onChange={handleChangeControlsMenuWidth} />
            <div className={styles.editor_container}>
                <Editor
                    links={links}
                    blocks={[...stages, ...blocks]}
                    onClickOnEmpty={handleClickOnEmpty}
                    onBlockRightClick={handleRightClick}
                    onBlockClick={handleBlockClick}
                    onDragStart={handleDragStart}
                    onMouseMove={handleMouseMove}
                    onDragEnd={handleDragEnd}
                    onInputClick={handleInputClick}
                    onOutputClick={handleOutputClick}
                />
                <EditorFooter
                    onSendToServer={execute}
                    onStepBack={back}
                    onIncreaseScale={() => {}}
                    onDecreaseScale={() => {}}
                    onSave={handleSave}
                />
            </div>
            <>
                <DragSplit left={false} onChange={handleSplitChangeResourcesMenuWidth} />
                <ResourcesMenu width={params.resourcesMenuWidth} resources={resources} />
            </>
            <>
                <DragSplit left={false} onChange={handleChangeBlockMenuWidth} />
                <BlockMenu width={params.blockParamsMenuWidth} block={menuBlock} />
            </>
            {!!blockPopupMenu && (
                <Popup
                    id={blockPopupMenu.id}
                    x={blockPopupMenu.x}
                    y={blockPopupMenu.y}
                    markAsStart={handleMarkAsStart}
                    markAsEnd={handleMarkAsEnd}
                    onExecute={handleExecucuteBlock }
                    onDelete={handleDeleteBlock}
                />
            )}
        </div>
  )
}

export default EditorPage

import { type PropsWithChildren } from 'react'
import AccountNav from '../../components/AccountNav'
import styles from './layout.module.scss'

type AccountLayoutProps = PropsWithChildren

const AccountLayout = ({ children }: AccountLayoutProps) => {
  return (
        <div className={styles.layout}>
            <AccountNav />
            {children}
        </div>
  )
}

export default AccountLayout

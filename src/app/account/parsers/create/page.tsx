import CreateParserForm from '../../../../components/CreateParserForm'

const CreateParser = async () => {
  return (
        <div>
            <p>Создание парсера</p>
            <CreateParserForm />
        </div>
  )
}

export default CreateParser

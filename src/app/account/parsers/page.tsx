'use client'

import axios from 'axios'
import Link from 'next/link'
import { useEffect, useState, useCallback } from 'react'

const Parsers = () => {
  const [parsers, setParsers] = useState([])

  const getParsers = useCallback(() => {
    axios.get('http://localhost:3030/parsers')
      .then(function (response) {
        setParsers(response.data)
      })
      .catch(function (error) {
        console.log(error)
      })
  }, [])

  useEffect(() => {
    getParsers()
  }, [])

  const handleDelete = (event: React.MouseEvent) => {
    const id = (event.target as HTMLDivElement).getAttribute('data-parser-id') as string

    axios.delete('http://localhost:3030/parsers', { data: { id } })
      .then(function (response) {
        getParsers()
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  return (
        <div>
            <p>Парсеры</p>
            {
                parsers.map((parser: { id: string, name: string }) => {
                  return (
                        <div key={parser.id} >
                            <Link href={`/editor/${parser.id}`}>{parser.name}</Link>||
                            <button data-parser-id={parser.id} onClick={handleDelete}>Удалить</button>
                        </div>
                  )
                })
            }
            <hr/>
            <Link href={'/account/parsers/create'}>Создать парсер</Link>
        </div>
  )
}

export default Parsers

import { type NextRequest, NextResponse } from 'next/server'
import Schema from '@/classes/Schema'
import Db from '@/classes/Db'

export async function POST (request: NextRequest, { params }: { params: { parserId: string } }) {
  const { parserId } = params

  try {
    const result = await Db.getParser(parserId)

    const parsed = JSON.parse(result.snapshot)
    const schema = new Schema([...parsed.blocks, ...parsed.stages])

    const serialized = await schema.execute()

    return NextResponse.json(serialized)
  } catch (err) {
    console.log(err)
    return new Response('Не удалось исполнить парсер', { status: 400 })
  }
}

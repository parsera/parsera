import Db from "@/classes/Db";
import { NextRequest } from "next/server";

export async function GET (request: NextRequest) {
    try {
        let list = await Db.getParsers()
        return list
    } catch (err) {
        console.log(err)
        return new Response('Ошибка сервера', {status:500})
    }
}

export async function POST (request: Request) {

}

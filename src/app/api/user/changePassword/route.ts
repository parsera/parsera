import Db from '@/classes/Db'
import { type NextRequest } from '../../../../../node_modules/next/server'
import ChangeUserDataDTO from '@/dto/ChangeUserDataDTO'

export async function POST(request: NextRequest) {

  const {login, Change} = await request.json()
  const  changeUserDataDTO = new ChangeUserDataDTO(login, Change)
  if (!Change) return new Response('Неверные данные', { status: 404 })
  try {
      await Db.changeUserData(changeUserDataDTO)
      return new Response('Изменено', {status:200})
  } catch (err) {
      console.log(err)
      return new Response('Ошибка сервера', {status:500})
  }
}

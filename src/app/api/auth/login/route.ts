import Db from '@/classes/Db'
import { type NextRequest } from '../../../../../node_modules/next/server'

export async function POST (request: NextRequest) {
  const { email, password } = await request.json()
  if (!email || !password) return new Response('Не удалось войти, проверьте данные', { status: 404 })
  try {
    const doc = await Db.getUser(email)
    if (!doc) return new Response('Пользователь не найден', { status: 400 })
    if (doc.password !== password) return new Response('Не удалось войти, проверьте данные', { status: 400 })
    return new Response('Успешный вход', { status: 200 })
  } catch (err) {
    console.log(err)
    return new Response('', { status: 500 })
  }
}

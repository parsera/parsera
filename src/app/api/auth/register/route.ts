import Db from '@/classes/Db'
import CreateUserDTO from '@/dto/CreateUserDTO'
import { type NextRequest } from '../../../../../node_modules/next/server'

export async function POST (request: NextRequest) {
  const { login, password } = await request.json()
  const createUserDTO = new CreateUserDTO(login, password)
  try {
    const doc = await Db.createUser(createUserDTO)
    return new Response('Успешная регистрация', { status: 200 })
  } catch (err) {
    console.log(err)
    return new Response('Ошибка входных данных', { status: 500 })
  }
}

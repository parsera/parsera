import { useEffect, useState } from "react"
import PluginManager from "../plugins"

const extendable = (componentName: string, Component: React.ComponentType) => {
    const Wrapper = (defaultProps: any) => {
        const [isLoading, setLoading] = useState(true)
        const [props, setProps] = useState(defaultProps)

        PluginManager.applyPluginsForComponentProps(componentName, defaultProps)

        useEffect(() => {
            PluginManager.getPropsAfterPluginsApplyed(componentName)
                .then((newProps) => {
                    setLoading(false)
                    setProps(newProps)
                }) 
                .catch((err) => {
                    console.log(err)
                })
        }, [])

        if(isLoading) return null

        return <Component {...props} />
    }

    return Wrapper
}

export default extendable
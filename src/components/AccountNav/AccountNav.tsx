'use client'
import Link from 'next/link'
import { List } from 'antd'
import styles from './styles.module.scss'

const accountPages = [
  {
    url: '/account',
    text: 'Главная'
  },
  {
    url: '/account/parsers',
    text: 'Парсеры'
  },
  {
    url: '/account/apis',
    text: 'Apis'
  }
]

const AccountNav = () => {
  const handleClick = () => {}

  return (
    <div className={styles.container}>
      <p>Добавить ступень</p>
      <List
        size="small"
        dataSource={accountPages}
        renderItem={({ url, text }) => (
          <List.Item className={styles.listItem}>
            <Link href={url}>{text}</Link>
          </List.Item>
        )}
      />
    </div>
  )
}

export default AccountNav

'use client'
import Link from 'next/link'
import styles from './styles.module.scss'
import { useEffect, useState } from 'react'
import axios from 'axios'

const Header = () => {
  const [user, setUser] = useState(null)
  useEffect(() => {
    axios.get('http://localhost:3030/me', { withCredentials: true })
      .then((res) => {
        setUser(res.data)
      })
      .catch((err) => { console.log(err) })
  }, [])

  return (
        <header className={styles.header}>
            <div className={styles.container}>
                <div className={styles.logo_conainer}>
                    <Link href={'/'}>Pushtuk</Link>
                </div>
                <nav className={styles.nav_container}>
                    <Link className={styles.nav_link} href="/help">Поддержка</Link>
                    <Link className={styles.nav_link} href="/forum">Форум</Link>
                    {!user
                      ? <Link className={styles.nav_link} href="/login">Вход</Link>
                      : <Link className={styles.nav_link} href="/account">Аккаунт</Link>
                    }
                </nav>
            </div>
        </header>
  )
}

export default Header

import React from 'react'
import type Point from '../../classes/Point'

interface BlockProps {
  id: string
  y: number
  x: number
  width: number
  height: number
  text: string
  inputXY: Point
  outputXY: Point
  type: string | undefined
  onClick: (event: React.MouseEvent) => void
  onRightClick: (event: React.MouseEvent) => void
  onDragStart: (event: React.MouseEvent) => void
  onDragEnd: (event: React.MouseEvent) => void
  onOutputClick: (event: React.MouseEvent) => void
  onInputClick: (event: React.MouseEvent) => void
}

const Block = ({ id, y, x, width, height, type, inputXY, outputXY, onClick, onRightClick, onDragStart, onDragEnd, onOutputClick, onInputClick }: BlockProps) => {
  return (
        <>
            <rect
                x={x}
                y={y}
                width={width}
                height={height}
                data-block-id={id}
                strokeWidth="2"
                rx="8"
                stroke="#00bfff"
                fill="transparent"
                onClick={onClick}
                // onMouseUp={onDragEnd}
                onMouseDown={onDragStart}
                onContextMenu={onRightClick}
            ></rect>
            {/* Инпут */}
            <rect
                x={inputXY.x}
                y={inputXY.y}
                width={20}
                height={20}
                data-block-id={id}
                fill='#ff0080'
                rx="10"
                stroke="red"
                onClick={onInputClick}
            ></rect>
            {/* Аутпут */}
            <rect
                x={outputXY.x}
                y={outputXY.y}
                data-block-id={id}
                width={20}
                height={20}
                fill='#00ff7f'
                rx="10"
                stroke="green"
                onClick={onOutputClick}
            ></rect>
            <text
                x={x + 8}
                y={y + 16}
                fontSize={16}
                // style={{ userSelect: 'none' }}
                pointerEvents='none'
            >{type}</text>
        </>
  )
}

export default Block

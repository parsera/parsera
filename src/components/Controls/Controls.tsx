import { List } from 'antd'
import { BlockTypes } from '../../constants'
import styles from './styles.module.scss'
import extendable from '../../hoc/extendable'
import { useEffect } from 'react'

interface ControlsProps {
  width: number
  onAddBlock: (blockType: BlockTypes) => void
}


const dataSource = [
  { blockType: BlockTypes.InitBrowser, name: 'Запустить браузер' },
  { blockType: BlockTypes.OpenPage, name: 'Открыть страницу' },
  { blockType: BlockTypes.TakeElementBySelector, name: 'Получить элемент' },
  { blockType: BlockTypes.ExtractInnerText, name: 'Получить текст' },
  { blockType: BlockTypes.MergeResults, name: 'Собрать результат' },
  { blockType: BlockTypes.ClosePage, name: 'Закрыть страницу' },
  { blockType: BlockTypes.CloseBrowser, name: 'Закрыть браузер' }
]

const GeneralBlocksList = (props: any) => {
  return (
    <>
        <p>Общее</p>
        <List {...props} />
    </>
  )
}

const GeneralBlocksListWrapped = extendable('GENERAL_BLOKS_LIST_COMPONENT', GeneralBlocksList)

const Controls = ({ width, onAddBlock }: ControlsProps) => {
  const handleAddBlock = (blockType: BlockTypes) => () => {
    onAddBlock(blockType)
  }

  return (
    <div className={styles.mainControllerBlock} style={{ minWidth: width }}>
      <div className={styles.controllerBlock}>
        <p>Специальное</p>
        <List
          size="small"
          dataSource={[{ blockType: BlockTypes.Stage, name: 'Добавить ступень' }]}
          renderItem={({ blockType, name }) => (
            <List.Item className={styles.listItem}>
              <button className={styles.listItemButton} onClick={handleAddBlock(blockType)}>
                {name}
              </button>
            </List.Item>
          )}
        />
        <GeneralBlocksListWrapped
          size="small"
          dataSource={[
            { blockType: BlockTypes.InitBrowser, name: 'Запустить браузер' },
            { blockType: BlockTypes.OpenPage, name: 'Открыть страницу' },
            { blockType: BlockTypes.TakeElementBySelector, name: 'Получить элемент' },
            { blockType: BlockTypes.ExtractInnerText, name: 'Получить текст' },
            { blockType: BlockTypes.MergeResults, name: 'Собрать результат' },
            { blockType: BlockTypes.ClosePage, name: 'Закрыть страницу' },
            { blockType: BlockTypes.CloseBrowser, name: 'Закрыть браузер' }
          ]}
          renderItem={({ blockType, name }: any) => (
            <List.Item className={styles.listItem}>
              <button className={styles.listItemButton} onClick={handleAddBlock(blockType)}>
                {name}
              </button>
            </List.Item>
          )}
        />
      </div>
    </div>
  )
}

export default Controls

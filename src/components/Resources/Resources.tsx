import { Table } from 'antd'
import styles from './styles.module.scss'
import Resources from '@/types/Resources'

const columns = [
  { title: 'id', dataIndex: 'id' },
  { title: 'Тип', dataIndex: 'type' }
]

interface ResourcesProps {
  resourcesMenuWidth: number
  resources: Resources
}

const Resources = ({ resourcesMenuWidth, resources }: ResourcesProps) => {
  return (
        <div className={styles.container} style={{ minWidth: resourcesMenuWidth }}>
            <p>Ресурсы:</p>
            <Table columns={columns} dataSource={resources} pagination={false} size='small' rowKey='id'/>
        </div>
  )
}

export default Resources

interface ArrowProps {
  x: number
  y: number
  angle: number
  length?: number
  width?: number
  sharpness?: number
}

const Arrow = ({ x, y, angle, length = 12, width = 5, sharpness = 5 }: ArrowProps) => {
  const x1 = x + length
  const y1 = y - width

  const x2 = x1 - sharpness
  const y2 = y

  const x3 = x1
  const y3 = y2 + width

  return (
        <path
            d={`M ${x} ${y} L ${x1} ${y1} L ${x2} ${y2} L ${x3} ${y3} Z`}
            fill="#148fb8"
            stroke="#148fb8"
            strokeMiterlimit="10"
            pointerEvents="all"
            transform={`rotate(${angle} ${x} ${y})`}
            ></path>
  )
}

export default Arrow

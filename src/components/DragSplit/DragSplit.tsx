'use client'
import { useState, type MouseEvent, Ref } from 'react'
import styles from './styles.module.scss'

interface DragSplitProps {
  left: boolean
  onChange: (width: number) => void
}

const DragSplit = ({ left, onChange }: DragSplitProps) => {
  const [isDrag, setDrag] = useState(false)

  const handleMouseDown = (event: MouseEvent<HTMLDivElement>) => {
    setDrag(true)
  }

  const handleMouseMove = (event: MouseEvent<HTMLDivElement>) => {
    if (isDrag) {
      console.log(event)
      if (left) {
        onChange(event.pageX)
      } else {
        onChange(document.documentElement.clientWidth - event.pageX)
      }
    }
  }

  const handleMouseUp = () => {
    setDrag(false)
  }

  return (
        <>
            { isDrag && (<div
                className={styles.dragSplit}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                ></div>) }
            <div className={styles.split} onMouseDown={handleMouseDown}></div>
        </>
  )
}

export default DragSplit

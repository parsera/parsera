'use client'
import { Button, Checkbox, Form, Input } from 'antd'
import axios from 'axios'
import { useRouter } from 'next/navigation'
import styles from './styles.module.scss'

interface FieldType {
  email?: string
  password?: string
  remember?: string
}

const LoginForm = () => {
  const router = useRouter()
  const handleFinish = (values: FieldType) => {
    console.log(values)
    const { email, password, remember } = values

    axios.post('http://localhost:3030/login', { email, password }, { withCredentials: true })
      .then(function (response) {
        console.log(response)
        router.push('/account')
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  const handleFinishFailed = () => {

  }

  return (
        <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            className={styles.form}
            initialValues={{ remember: true }}
            onFinish={handleFinish}
            onFinishFailed={handleFinishFailed}
            autoComplete="off"
        >
            <Form.Item<FieldType>
            label="email"
            name="email"
            rules={[{ required: true, message: 'Please input your email!' }]}
            >
            <Input />
            </Form.Item>

            <Form.Item<FieldType>
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}
            >
            <Input.Password />
            </Form.Item>

            <Form.Item<FieldType>
            name="remember"
            valuePropName="checked"
            wrapperCol={{ offset: 8, span: 16 }}
            >
            <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </Form.Item>
        </Form>
  )
}

export default LoginForm

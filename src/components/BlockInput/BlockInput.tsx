interface BlockInputProps {
  x: number
  y: number
  blockId: string
  onClick: (event: React.MouseEvent) => void
}

const BlockInput = ({ x, y, blockId, onClick }: BlockInputProps) => {
  return (
        <rect
            x={x}
            y={y}
            width={20}
            height={20}
            data-block-id={blockId}
            fill='red'
            rx="10"
            stroke="red"
            onClick={onClick}
        ></rect>
  )
}

export default BlockInput

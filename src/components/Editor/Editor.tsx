'use client'
import React, { useRef, useState, useLayoutEffect } from 'react'
import Block from '../Block'
import Line from '../Line'
import type UtilBlock from '@/app/classes/Block'
import type UtilLink from '@/app/classes/Link'

interface EditorProps {
  links: UtilLink[]
  blocks: UtilBlock[]
  onClickOnEmpty: (event: React.MouseEvent) => void
  onBlockRightClick: (event: React.MouseEvent) => void
  onBlockClick: (event: React.MouseEvent) => void
  onDragStart: (event: React.MouseEvent) => void
  onDragEnd: (event: React.MouseEvent) => void
  onMouseMove: (event: React.MouseEvent) => void
  onOutputClick: (event: React.MouseEvent) => void
  onInputClick: (event: React.MouseEvent) => void
}

const Editor = ({ links, blocks, onClickOnEmpty, onBlockRightClick, onBlockClick, onDragStart, onMouseMove, onDragEnd, onOutputClick, onInputClick }: EditorProps) => {
  return (
        <div style={{ width: '100%', flexBasis: '100%' }}>
            <svg
                style={{ position: 'absolute', left: 0, top: 0, width: '100%', height: '100%' }}
                xmlns="http://www.w3.org/2000/svg"
                // className="w-full h-full"
                onClick={onClickOnEmpty}
                onMouseMove={onMouseMove}
                onMouseUp={onDragEnd}
            >
                {
                    blocks.map((value) => {
                      const { id, x, y, width, height, inputXY, outputXY, text, type } = value
                      return (
                            <Block
                                key={id}
                                id={id}
                                x={x}
                                y={y}
                                width={width}
                                height={height}
                                inputXY={inputXY}
                                outputXY={outputXY}
                                text={text}
                                type={type}
                                onClick={onBlockClick}
                                onRightClick={onBlockRightClick}
                                onDragEnd={onDragEnd}
                                onDragStart={onDragStart}
                                onOutputClick={onOutputClick}
                                onInputClick={onInputClick}
                            />
                      )
                    })
                }
                {
                    links.map((value, index) => {
                      const [end1, end2] = value.ends
                      return (
                            <Line key={index} end1={end1} end2={end2} />
                      )
                    })
                }
            </svg>
        </div>
  )
}

export default Editor

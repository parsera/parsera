const block1 = {
  id: 'startStage',
  x: 300,
  y: 30,
  width: 250,
  height: 100,
  text: 'Блок1',
  type: 'StartNode',
  next: [],
  userParams: { url: 'https://romanpushtuk.github.io/cv-page/' },
  elements: []
}

const block2 = {
  id: '2',
  x: 300,
  y: 200,
  width: 250,
  height: 100,
  text: 'Блок2',
  type: 'TakeElemBySelectorNode',
  next: [],
  userParams: { selector: '.header-info_header-name' },
  elements: []
}

const block3 = {
  id: '3',
  x: 300,
  y: 380,
  width: 250,
  height: 100,
  text: 'Блок3',
  type: 'ExtractTextFromSelectorNode',
  next: [],
  userParams: {},
  elements: []
}

const block4 = {
  id: '4',
  x: 300,
  y: 560,
  width: 250,
  height: 100,
  text: 'Блок3',
  type: 'CollectResultNode',
  next: [],
  userParams: {},
  elements: []
}

const link = {
  input: { id: '1', x: 10, y: 20 },
  output: { id: '2', x: 50, y: 70 }
}

const shema = [block1, block2, block3, block4]

export default shema

import styles from './styles.module.scss'

const MainSection = () => {
  return (
        <section className={styles.section}>
            <div className={styles.main_section_container}>
                <div className={styles.main_info_container}>
                    <h2>Pushtuk может парсить информацию с сайтов без кода</h2>
                    <p>Автоматизируйте любые задачи в интернете. Повысьте эффективность своего бизнеса уже сегодня с автоматизацией рутинных задач.</p>
                    <div>
                        <button>
                            Попробовать
                        </button>
                    </div>
                </div>
                <div className={styles.main_info_img_container}>
                    <img alt="parser_screenshot" />
                </div>
            </div>
        </section>
  )
}

export default MainSection

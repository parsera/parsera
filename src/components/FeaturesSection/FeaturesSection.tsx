import styles from './styles.module.scss'

const FeaturesSection = () => {
  return (
        <section className={styles.section}>
            <div className={styles.container}>
                <div>
                    <h2>Основные возможности Pushtuk</h2>
                </div>
                <div className={styles.greed_features}>
                    <div>
                        <h4>Блок схема</h4>
                        <p>Создайте в графическом редакторе схему парсинга сайтов из блоков</p>
                    </div>
                    <div>
                        <h4>Создайте API</h4>
                        <p>Вы можете вызвать созданный парсер посредством запроса к нашему REST API, это позволяет легко встроить парсер в любые ваши программные решения.</p>
                    </div>
                    <div>
                        <h4>Запуск на своем сервере</h4>
                        <p>Есть возможность получить сгенерированный парсер для запуска на собственном сервере, это дает вам уверенность в том</p>
                    </div>
                    <div>
                        <h4>Интеграция</h4>
                        <p>Возможность взаимодейтсвия с множеством полезных вспомогательных сервисов</p>
                    </div>
                    <div>
                        <h4>Puppeter под капотом</h4>
                        <p>Pushtuk написан на Node.js и использованием библиотеки Puppeter.</p>
                    </div>
                    <div>
                        <h4>AI иструменты</h4>
                        <p>Возможность применять иструменты искуственного интеллекта</p>
                    </div>
                </div>
            </div>
        </section>
  )
}

export default FeaturesSection

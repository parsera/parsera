interface ConnectorProps {
  blockId: string
  x: number
  y: number
  fill: string
  onClick: (event: React.MouseEvent) => void
}

const Connector = ({ x, y, blockId, fill = 'red', onClick }: ConnectorProps) => {
  return (
        <rect
            x={x}
            y={y}
            width={20}
            height={20}
            data-block-id={blockId}
            fill={fill}
            strokeWidth="10"
            stroke="red"
            onClick={onClick}
        />
  )
}

export default Connector

import React from 'react'
import { List } from 'antd'
import styles from './styles.module.scss'

interface PopupProps {
  id: string
  x: number
  y: number
  onExecute: (id: string) => void
  markAsStart: (id: string) => void
  markAsEnd: (id: string) => void
  onDelete: (id: string) => void
}

const Popup = React.memo(
  ({ id, x, y, onExecute, markAsStart, markAsEnd, onDelete }: PopupProps) => {
    const handleExecute = () => { onExecute(id) }

    const handleMarkAsStart = () => { markAsStart(id) }

    const handleMarkAsEnd = () => { markAsEnd(id) }

    const handleDelete = () => { onDelete(id) }

    return (
      <div className={styles.container} style={{ top: y, left: x }}>
        <List
          className={styles.list}
          size="small"
          bordered
          dataSource={[
            <button onClick={handleExecute} className={styles.popUpButton}>
              Выполнить ▶
            </button>,
            <button onClick={handleMarkAsStart} className={styles.popUpButton}>
              Пометить (start)
            </button>,
            <button onClick={handleMarkAsEnd} className={styles.popUpButton}>
              Пометить (end)
            </button>,
            <button className={styles.popUpButton} disabled>
              На передний план
            </button>,
            <button className={styles.popUpButton} disabled>
              На задний план
            </button>,
            <button className={styles.popUpButton} onClick={handleDelete}>
              Удалить 🧺
            </button>
          ]}
          renderItem={(item) => <List.Item>{item}</List.Item>}
        />
      </div>
    )
  }
)

export default Popup

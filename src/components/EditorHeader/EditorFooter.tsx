import styles from './styles.module.scss'

interface EditorFooter {
  onSendToServer: () => void
  onStepBack: () => void
  onIncreaseScale: () => void
  onDecreaseScale: () => void
  onSave: () => void
}

const EditorFooter = ({
  onSendToServer,
  onStepBack,
  onIncreaseScale,
  onDecreaseScale,
  onSave
}: EditorFooter) => {
  return (
    <div className={styles.editorFooterMainBlock}>
      <div className={styles.editorFooterBlock}>
        <div className={styles.buttonBlock}>
          <button onClick={onStepBack} className={styles.changeButton}>
            Шаг назад
          </button>
        </div>
        <div className={styles.buttonBlock}>
          <button disabled onClick={onIncreaseScale} className={styles.changeButton}>
            Увеличить маштаб +
          </button>
        </div>
        <div className={styles.buttonBlock}>
          <button disabled onClick={onDecreaseScale} className={styles.changeButton}>
            Уменьшить маштаб -
          </button>
        </div>
        <div className={styles.buttonBlock}>
          <button onClick={onSendToServer} className={styles.changeButton}>
            Запустить парсинг ▶
          </button>
        </div>
        <div className={styles.buttonBlock}>
          <button onClick={onSave} className={styles.changeButton}>
            Сохранить
          </button>
        </div>
      </div>
    </div>
  )
}

export default EditorFooter

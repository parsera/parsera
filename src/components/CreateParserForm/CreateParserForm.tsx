'use client'
import { Form, Input } from 'antd'
import axios from 'axios'

const CreateParserForm = () => {
  const [form] = Form.useForm()

  const handleFinish = (values: any) => {
    axios.post('http://localhost:3030/parsers', values)
      .then(function (response) {
        console.log(response)
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  return (
        <Form
            form={form}
            onFinish={handleFinish}
        >
            <Form.Item label="Название" name='name'>
                <Input size='small' placeholder="Название парсера" />
            </Form.Item>
            <Form.Item>
                <button onClick={handleFinish}>Создать</button>
            </Form.Item>
        </Form>
  )
}

export default CreateParserForm

'use client'
import { Input } from 'antd'
import type Block from '../../classes/Block'
import Editor from '../../classes/Editor'
import ChangeBlockDefaultParamsCommand from '../../classes/commands/ChangeBlockDefaultParamsCommand'
import ChangeBlockUserParamsCommand from '../../classes/commands/ChangeBlockUserParamsCommand'
import styles from './styles.module.scss'

interface MenuProps {
  width: number
  block: Block | null
  onClose: () => void
}

const defaultParamKeys: Array<{ key: keyof Block, type: 'number' | 'checkbox' }> = [
  {
    key: 'x',
    type: 'number'
  },
  {
    key: 'y',
    type: 'number'
  },
  {
    key: 'width',
    type: 'number'
  },
  {
    key: 'height',
    type: 'number'
  },
  {
    key: 'start',
    type: 'checkbox'
  },
  {
    key: 'end',
    type: 'checkbox'
  }
]

const Menu = ({ width, block, onClose }: MenuProps) => {
  if (!block) return null

  const handleChangeBlockParams = (event: React.ChangeEvent<HTMLInputElement>) => {
    const keyName = (event.target as HTMLDivElement).getAttribute('data-key-name') as string
    const type = (event.target as HTMLDivElement).getAttribute('data-type') as string

    let value = null

    if (type == 'checkbox') {
      value = Boolean(event.target.checked)
    }

    if (type == 'number') {
      value = Number(event.target.value)
    }

    const command = new ChangeBlockDefaultParamsCommand(block.id, keyName, value as boolean | number)

    Editor.executeCommand(command)
  }

  const handleChangeUserParams = (event: React.ChangeEvent<HTMLInputElement>) => {
    const keyName = (event.target as HTMLDivElement).getAttribute('data-key-name') as string
    const type = (event.target as HTMLDivElement).getAttribute('data-type') as string

    let value = null

    if (type == 'text') {
      value = String(event.target.value)
    }

    const command = new ChangeBlockUserParamsCommand(block.id, keyName, value as string)
    Editor.executeCommand(command)
  }

  const renderDefaultBlockParams = () => {
    return defaultParamKeys.map(({ key, type }) => {
      const inputProps = {
        ...(type === 'number' ? { value: block[key] as number, type: 'number' } : {}),
        ...(type === 'checkbox' ? { value: block[key] as boolean, type: 'checkbox' } : {})
      }

      return (
                // @ts-expect-error
                <Input
                    key={key}
                    size="small"
                    addonBefore={key}
                    {...inputProps}
                    data-key-name={key}
                    data-type={type}
                    onChange={handleChangeBlockParams}
                />
      )
    })
  }

  const renderUserParams = () => {
    return Object.entries(block.command.userParams).map(([key, value], index) => {
      return (
                <Input
                    key={index}
                    size="small"
                    type="text"
                    data-type={'text'}
                    addonBefore={key}
                    data-key-name={key}
                    value={value}
                    onChange={handleChangeUserParams}
                />
      )
    })
  }

  return (
        <div className={styles.menuMainBlock}>
            <div className={styles.menuBlock}>
                <div>
                    <button onClick={onClose} className={styles.menuCloseButton}>Закрыть меню</button>
                </div>
                <div>
                    <p>Стандартные параметры:</p>
                    {renderDefaultBlockParams()}
                </div>
                <div>
                    <p>Пользовательские параметры:</p>
                    {renderUserParams()}
                </div>
            </div>
        </div>
  )
}

export default Menu

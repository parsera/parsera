import type End from '@/app/classes/End'
import Arrow from '../Arrow'

interface LineProps {
  end1: End
  end2: End
}

const Line = ({ end1, end2 }: LineProps) => {
  const input = end1.type == 'input' ? end1 : end2
  const output = end1.type !== 'input' ? end1 : end2

  const { x: x1, y: y1 } = input
  const { x: x2, y: y2 } = output

  const angle = (180 - Math.atan2(y1 - y2, x1 - x2) * (180 / 3.14)) * -1

  return (
        <>
            <Arrow x={x1} y={y1} angle={angle} />
            <line x1={x1} y1={y1} x2={x2} y2={y2} stroke="#148fb8" strokeWidth={2} />
        </>
  )
}

export default Line

import axios from 'axios'

const saveParser = async (id: string, snapshot: any) => {
  return await axios.put(`http://localhost:3030/parsers/${id}`, { snapshot })
}

export default saveParser

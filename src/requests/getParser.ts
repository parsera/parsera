import axios from 'axios'

const getParser = async (id: string) => {
  return await axios.get(`http://localhost:3030/parsers/${id}`)
}

export default getParser

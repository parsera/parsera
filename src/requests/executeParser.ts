import axios from 'axios'
import type Resources from '../types/Resources'

const executeParser = async (id: string): Promise<Resources> => {
  return await axios.post(`http://localhost:3030/parse/${id}`)
}

export default executeParser

export default function plugin(componentName: string, props: any) {
    if (componentName === 'GENERAL_BLOKS_LIST_COMPONENT') {
        props.dataSource.push({ blockType: 'AI_PROCESS', name: 'AI процессинг' })
        return { 'GENERAL_BLOKS_LIST_COMPONENT': props  }
    }
}
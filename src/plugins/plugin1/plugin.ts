import Block from "../../classes/Block"

class CallHttpBlock extends Block {
    constructor (...params: any[]) {
      super('CALL_HTTP', ...params)
      this.command.resourceType = 'response'
      this.command.code = '(function () { return async function callHttp (resouces, userParams) {' +
                                  'return "{}"' +
                              '} })();'
    }
}


export default function plugin(componentName: string, props: any) {
    if (componentName === 'GENERAL_BLOKS_LIST_COMPONENT') {
        props.dataSource.push({ blockType: 'CALL_HTTP', name: 'HTTP вызов' })
        return { ...props }
    }
}
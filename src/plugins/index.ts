const plugins = [
    import('./plugin1/plugin'),
    import('./plugin2/plugin')
]

class PluginManager {
    static propsMap: {[key: string]: any} = {}
    static subscribers: { name: string, emiter: Function }[] = []

    static async applyPluginsForComponentProps(componentName: string, props: any) {
        console.log('Применили плагины')

        for(let pluginImportModule of plugins) {
            const plugin = await pluginImportModule
            const newProps = plugin.default(componentName, PluginManager.propsMap[componentName] ?? props)

            if (newProps) {
                PluginManager.propsMap = { ...PluginManager.propsMap, ...newProps }
            }
        }

        PluginManager.emit()
    }

    static getPropsAfterPluginsApplyed(componentName: string) {
        return new Promise((resolve) => {
            const emiter = (newProps: any) => resolve(newProps)
            PluginManager.subscribers.push({ name: componentName, emiter })
        })
    }

    static emit() {
        PluginManager.subscribers.forEach(({ name, emiter }) => {
            const newProps = PluginManager.propsMap[name]
            emiter(newProps)
        })
    }
}

export default PluginManager
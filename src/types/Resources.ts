interface Resource { id: string }

type Resources = Record<string, Resource>

export type { Resource }
export default Resources

interface EditorParams {
  blockListMenuWidth: number
  resourcesMenuWidth: number
  blockParamsMenuWidth: number
}

export default EditorParams

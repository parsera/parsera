import { useCallback, useEffect, useState } from 'react'
import { useParams } from 'next/navigation'
import { type BlockTypes } from '../constants'
import getParser from '../requests/getParser'
import executeParser from '../requests/executeParser'
import saveParser from '../requests/safeParser'
import EditorHistory from '../classes/EditorHistory'
import Editor, { type Subscriber } from '../classes/Editor'
import { type StageBlock } from '../classes/Block'
import type Block from '../classes/Block'
import type Link from '../classes/Link'
import Snapshot from '../classes/Snapshot'
import type EditorParams from '../types/EditorParams'
import type Resources from '../types/Resources'
import AddBlockCommand from '../classes/commands/AddBlockCommand'
import AddLinkCommand from '../classes/commands/AddLinkCommand'
import MoveBlockCommand from '../classes/commands/MoveBlockCommand'
import AddBlockToStage from '../classes/commands/AddBlockToStageCommand'
import ExecuteBlockCommand from '../classes/commands/ExecuteBlockCommand'
import DeleteBlockCommand from '../classes/commands/DeleteBlockCommand'

type UseEditor = () => {
  err: Error | null
  resources: Resources | null
  params: EditorParams
  blocks: Block[]
  stages: StageBlock[]
  links: Link[]
  back: () => void
  execute: () => void
  executeBlock: (id: string) => void
  addBlock: (blockType: BlockTypes) => void
  deleteBlock: (id: string) => void
  markAsStart: (id: string) => void
  markAsEnd: (id: string) => void
  addLink: (inputId: string, inputX: number, inputY: number, outputId: string, outputX: number, outputY: number) => void
  moveBlock: (blockId: string, x: number, y: number) => void
  addBlockToStage: (stageId: string, blockId: string) => void
  save: () => void
}

const useEditor: UseEditor = () => {
  const router = useParams()
  const [err, setError] = useState<Error | null>(null)
  const [resources, setResources] = useState<Resources | null>(null)
  const [params, setParams] = useState<EditorParams>({ ...Editor.params })
  const [blocks, setBlocks] = useState<Block[]>([])
  const [stages, setStages] = useState<StageBlock[]>([])
  const [links, setLinks] = useState<Link[]>([])

  useEffect(() => {
    getParser(router.id as string)
      .then((response) => {
        const { blocks, stages, links } = JSON.parse(response.data.snapshot)
        const snapshot = new Snapshot(blocks, stages, links)
        Editor.restore(snapshot)
      })
      .catch((err: Error) => {
        console.log(err)
        setError(err)
      })

    const onEditorChange: Subscriber = ({ params, blocks, links, stages }) => {
      setParams({ ...params })
      setStages([...stages])
      setBlocks([...blocks])
      setLinks([...links])
    }

    Editor.subscribe(onEditorChange)

    return () => {
      Editor.unsubscribe(onEditorChange)
    }
  }, [])

  const back = useCallback(() => {
    const command = EditorHistory.getLastCommand()
    if (command) {
      command.undo()
    }
  }, [])

  const execute = useCallback(() => {
    executeParser(router.id as string)
      .then(setResources)
      .catch((err: Error) => {
        console.log(err)
        setError(err)
      })
  }, [])

  const executeBlock = useCallback((id: string) => {
    const command = new ExecuteBlockCommand(id)
    Editor.executeCommand(command)
  }, [])

  const deleteBlock = useCallback((id: string) => {
    const command = new DeleteBlockCommand(id)
    Editor.executeCommand(command)
  }, [])

  const addBlock = useCallback((blockType: BlockTypes) => {
    const command = new AddBlockCommand(blockType)
    Editor.executeCommand(command)
  }, [])

  const addLink = useCallback((inputId: string, inputX: number, inputY: number, outputId: string, outputX: number, outputY: number) => {
    const command = new AddLinkCommand(inputId, inputX, inputY, outputId, outputX, outputY)
    Editor.executeCommand(command)
  }, [])

  const moveBlock = useCallback((blockId: string, x: number, y: number) => {
    const command = new MoveBlockCommand(blockId, x, y)
    Editor.executeCommand(command)
  }, [])

  const addBlockToStage = useCallback((stageId: string, blockId: string) => {
    const command = new AddBlockToStage(stageId, blockId)
    Editor.executeCommand(command)
  }, [])

  const save = useCallback(() => {
    const snapshot = Editor.createSnapshot()
    const jsonSnapshot = JSON.stringify(snapshot.getState())
    saveParser(router.id as string, jsonSnapshot)
      .catch((err) => {
        console.log(err)
        setError(err)
      })
  }, [])

  // TODO - сделать командой
  const markAsStart = useCallback((id: string) => {
    const stage = Editor.getStage(id)
    if (stage) {
      stage.start = true
    }
  }, [])

  // TODO - сделать командой
  const markAsEnd = useCallback((id: string) => {
    const stage = Editor.getStage(id)
    if (stage) {
      stage.end = true
    }
  }, [])

  return {
    err,
    resources,
    params,
    blocks,
    stages,
    links,
    back,
    execute,
    executeBlock,
    addBlock,
    deleteBlock,
    markAsStart,
    markAsEnd,
    addLink,
    moveBlock,
    addBlockToStage,
    save
  }
}

export default useEditor
